exports.id = 498;
exports.ids = [498];
exports.modules = {

/***/ 3545:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "_": () => (/* binding */ Context)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8038);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

const Context = /*#__PURE__*/ react__WEBPACK_IMPORTED_MODULE_0___default().createContext();


/***/ }),

/***/ 1978:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ Layout)
});

// EXTERNAL MODULE: external "next/dist/compiled/react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(6786);
// EXTERNAL MODULE: external "next/dist/compiled/react"
var react_ = __webpack_require__(8038);
// EXTERNAL MODULE: ./src/app/admin/SidebarItem.module.css
var SidebarItem_module = __webpack_require__(2641);
var SidebarItem_module_default = /*#__PURE__*/__webpack_require__.n(SidebarItem_module);
// EXTERNAL MODULE: ./node_modules/next/link.js
var next_link = __webpack_require__(1621);
var link_default = /*#__PURE__*/__webpack_require__.n(next_link);
;// CONCATENATED MODULE: ./src/app/admin/SidebarItem.js



function SidebarItem({ item , Icon , isActive , handleClick , handleSubItemClick , activeSubItem  }) {
    const link = item.items ? `/admin/${item.title}/${item.items[0].title}` : `/admin/${item.title}`;
    return /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
        children: [
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)((link_default()), {
                href: link,
                className: `${(SidebarItem_module_default()).label} ${isActive ? (SidebarItem_module_default()).labelactive : ""}`,
                onClick: ()=>handleClick(item),
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx(Icon, {}),
                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        className: (SidebarItem_module_default()).title,
                        children: item.title
                    })
                ]
            }),
            item.items && isActive && /*#__PURE__*/ jsx_runtime_.jsx("div", {
                className: (SidebarItem_module_default()).children,
                children: item.items.map((subItem)=>/*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                        href: `/admin/${item.title}/${subItem.title}`,
                        onClick: ()=>handleSubItemClick(subItem.title),
                        className: `${(SidebarItem_module_default()).item} ${subItem.title === activeSubItem ? (SidebarItem_module_default()).itemactive : ""}`,
                        children: subItem.title
                    }, subItem.title))
            })
        ]
    });
}
/* harmony default export */ const admin_SidebarItem = (SidebarItem);

// EXTERNAL MODULE: ./node_modules/react-icons/md/index.esm.js
var index_esm = __webpack_require__(4348);
// EXTERNAL MODULE: ./node_modules/next/navigation.js
var navigation = __webpack_require__(9483);
;// CONCATENATED MODULE: ./src/app/admin/Sidebar.js





function Sidebar() {
    const currentUrl = (0,navigation.usePathname)();
    const currentItem = currentUrl.split("/")[2];
    const currentSubItem = currentUrl.split("/")[3];
    const [activeItem, setActiveItem] = (0,react_.useState)(currentItem || menu[0].title);
    const [activeSubItem, setActiveSubItem] = (0,react_.useState)(currentSubItem || null);
    const handleClick = (item)=>{
        if (activeItem === item.title) return;
        setActiveItem(item.title);
        if (item.items) setActiveSubItem(item.items[0].title);
    };
    const handleSubItemClick = (title)=>{
        setActiveSubItem(title);
    };
    return /*#__PURE__*/ jsx_runtime_.jsx("div", {
        style: {
            display: "flex",
            flexDirection: "column",
            width: "100%"
        },
        children: menu.map((item)=>/*#__PURE__*/ jsx_runtime_.jsx(admin_SidebarItem, {
                item: item,
                Icon: item.Icon,
                isActive: item.title === activeItem,
                activeSubItem: activeSubItem,
                handleClick: handleClick,
                handleSubItemClick: handleSubItemClick
            }, item.title))
    });
}
const menu = [
    {
        Icon: index_esm/* MdDashboard */.bUq,
        title: "dashboard"
    },
    {
        Icon: index_esm/* MdSchool */.eAf,
        title: "platform",
        items: [
            {
                title: "certificates"
            },
            {
                title: "exams"
            },
            {
                title: "tutorials"
            },
            {
                title: "slides"
            },
            {
                title: "quizzes"
            }
        ]
    },
    {
        Icon: index_esm/* MdPeople */.J8I,
        title: "customers",
        items: [
            {
                title: "courses"
            },
            {
                title: "attempts"
            },
            {
                title: "answers"
            }
        ]
    },
    {
        Icon: index_esm/* MdPerson */.Vyx,
        title: "users"
    }
];
/* harmony default export */ const admin_Sidebar = (Sidebar);

// EXTERNAL MODULE: ./node_modules/next/image.js
var next_image = __webpack_require__(8421);
var image_default = /*#__PURE__*/__webpack_require__.n(next_image);
// EXTERNAL MODULE: ./src/app/admin/layout.module.css
var layout_module = __webpack_require__(7596);
var layout_module_default = /*#__PURE__*/__webpack_require__.n(layout_module);
// EXTERNAL MODULE: ./src/connect/qpbe.js
var qpbe = __webpack_require__(4352);
;// CONCATENATED MODULE: ./src/app/admin/Avatar.js



async function Avatar({ accessToken  }) {
    const response = await (0,qpbe/* getProfile */.Ai)(accessToken);
    return /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
        style: {
            display: "flex",
            flexDirection: "column",
            gap: "1rem",
            alignItems: "center"
        },
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx((image_default()), {
                src: response.data.avatar_url,
                alt: "Admin Avatar",
                width: 100,
                height: 100,
                priority: true,
                style: {
                    borderRadius: "100%"
                }
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                children: response.data.username
            })
        ]
    });
}
/* harmony default export */ const admin_Avatar = (Avatar);

// EXTERNAL MODULE: ./node_modules/js-cookie/dist/js.cookie.mjs
var js_cookie = __webpack_require__(7270);
;// CONCATENATED MODULE: ./src/app/admin/Logout.js



const Logout = ()=>{
    const router = (0,navigation.useRouter)();
    const handleClick = ()=>{
        js_cookie/* default.remove */.Z.remove("access");
        js_cookie/* default.remove */.Z.remove("refresh");
        js_cookie/* default.remove */.Z.remove("role");
        router.push("/auth/signin");
    };
    return /*#__PURE__*/ jsx_runtime_.jsx("button", {
        onClick: handleClick,
        children: "Logout"
    });
};
/* harmony default export */ const admin_Logout = (Logout);

// EXTERNAL MODULE: ./src/app/admin/context.js
var admin_context = __webpack_require__(3545);
;// CONCATENATED MODULE: ./src/app/admin/layout.js










function Layout({ children  }) {
    const [context, setContext] = (0,react_.useState)({});
    (0,react_.useEffect)(()=>{
        const role = js_cookie/* default.get */.Z.get("role");
        const accessToken = js_cookie/* default.get */.Z.get("access");
        setContext({
            role: role,
            accessToken: accessToken
        });
    }, []);
    if (!context.role) return /*#__PURE__*/ jsx_runtime_.jsx(jsx_runtime_.Fragment, {});
    else if (context.role !== "admin" && context.role !== "manager" && context.role !== "contributor") return /*#__PURE__*/ jsx_runtime_.jsx("div", {
        className: (layout_module_default()).block,
        children: /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
            href: "/auth/signin",
            children: /*#__PURE__*/ jsx_runtime_.jsx("button", {
                children: "You don't have access, go back to sign in"
            })
        })
    });
    return /*#__PURE__*/ jsx_runtime_.jsx(admin_context/* Context.Provider */._.Provider, {
        value: [
            context,
            setContext
        ],
        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
            className: (layout_module_default()).admin,
            children: [
                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: (layout_module_default()).header,
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: (layout_module_default()).menucorner,
                            children: /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                children: "Quizzpool"
                            })
                        }),
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: (layout_module_default()).mainheader,
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx((image_default()), {
                                    src: "/images/logo.png",
                                    alt: "Logo",
                                    width: 48,
                                    height: 48
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                    className: (layout_module_default()).btns,
                                    children: /*#__PURE__*/ jsx_runtime_.jsx(admin_Logout, {})
                                })
                            ]
                        })
                    ]
                }),
                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: (layout_module_default()).body,
                    children: [
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: (layout_module_default()).sidebar,
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                    className: (layout_module_default()).profile,
                                    children: /*#__PURE__*/ jsx_runtime_.jsx(admin_Avatar, {
                                        accessToken: context.accessToken
                                    })
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                    className: (layout_module_default()).hr,
                                    children: /*#__PURE__*/ jsx_runtime_.jsx("hr", {})
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                    className: (layout_module_default()).menu,
                                    children: /*#__PURE__*/ jsx_runtime_.jsx(admin_Sidebar, {})
                                })
                            ]
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: (layout_module_default()).content,
                            children: children
                        })
                    ]
                })
            ]
        })
    });
}


/***/ }),

/***/ 4476:
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

Promise.resolve(/* import() eager */).then(__webpack_require__.bind(__webpack_require__, 1978))

/***/ }),

/***/ 2641:
/***/ ((module) => {

// Exports
module.exports = {
	"label": "SidebarItem_label__b2ROd",
	"labelactive": "SidebarItem_labelactive__mw1sU",
	"title": "SidebarItem_title__Q85Vi",
	"children": "SidebarItem_children__yuH4N",
	"item": "SidebarItem_item__lsjDN",
	"itemactive": "SidebarItem_itemactive____LBW"
};


/***/ }),

/***/ 7596:
/***/ ((module) => {

// Exports
module.exports = {
	"block": "layout_block__r2LpX",
	"admin": "layout_admin__12rl1",
	"header": "layout_header__lC2xX",
	"body": "layout_body__qE8SQ",
	"menucorner": "layout_menucorner__KiI3Y",
	"mainheader": "layout_mainheader__g2t8n",
	"sidebar": "layout_sidebar__pBoAV",
	"hr": "layout_hr__NSh70",
	"profile": "layout_profile__AYY7x",
	"menu": "layout_menu__TIVqR",
	"name": "layout_name__n91Ie",
	"content": "layout_content__s0FbJ"
};


/***/ }),

/***/ 5734:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "$$typeof": () => (/* binding */ $$typeof),
/* harmony export */   "__esModule": () => (/* binding */ __esModule),
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var next_dist_build_webpack_loaders_next_flight_loader_module_proxy__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5985);

const proxy = (0,next_dist_build_webpack_loaders_next_flight_loader_module_proxy__WEBPACK_IMPORTED_MODULE_0__.createProxy)("/home/hqtrung/code/quizzpool_fe/src/app/admin/layout.js")

// Accessing the __esModule property and exporting $$typeof are required here.
// The __esModule getter forces the proxy target to create the default export
// and the $$typeof value is for rendering logic to determine if the module
// is a client boundary.
const { __esModule, $$typeof } = proxy;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (proxy.default);


/***/ })

};
;