"use strict";
exports.id = 352;
exports.ids = [352];
exports.modules = {

/***/ 4352:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Ai": () => (/* binding */ getProfile),
/* harmony export */   "CT": () => (/* binding */ getSlide),
/* harmony export */   "Dm": () => (/* binding */ getExamList),
/* harmony export */   "L6": () => (/* binding */ deleteTutorial),
/* harmony export */   "OM": () => (/* binding */ getExam),
/* harmony export */   "PW": () => (/* binding */ getQuizList),
/* harmony export */   "Qf": () => (/* binding */ deleteSlide),
/* harmony export */   "Wr": () => (/* binding */ getListCertificate),
/* harmony export */   "ZH": () => (/* binding */ deleteExam),
/* harmony export */   "_y": () => (/* binding */ getSlideList),
/* harmony export */   "c0": () => (/* binding */ resetPassword),
/* harmony export */   "ck": () => (/* binding */ getTutorial),
/* harmony export */   "gF": () => (/* binding */ forgotPassword),
/* harmony export */   "mv": () => (/* binding */ getCertificate),
/* harmony export */   "s6": () => (/* binding */ getQuiz),
/* harmony export */   "vl": () => (/* binding */ getTutorialList),
/* harmony export */   "vn": () => (/* binding */ deleteCertificate),
/* harmony export */   "x4": () => (/* binding */ login),
/* harmony export */   "y3": () => (/* binding */ deleteQuiz),
/* harmony export */   "z2": () => (/* binding */ register)
/* harmony export */ });
/* unused harmony exports refreshToken, getUserList */
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5085);

const api = axios__WEBPACK_IMPORTED_MODULE_0__/* ["default"].create */ .Z.create({
    baseURL: "https://quizzpool.smarthand.dev/api/"
});
const register = async (userData)=>{
    try {
        return await api.post("register", userData);
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const login = async (email, password)=>{
    try {
        return await api.post("login", {
            email,
            password
        });
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const refreshToken = async (refreshToken)=>{
    try {
        return await api.post("refresh_token", {
            refresh: refreshToken
        });
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const forgotPassword = async (email)=>{
    try {
        return await api.post("forgot_password", {
            email
        });
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const resetPassword = async (password, accessToken)=>{
    try {
        return await api.post("reset_password", {
            password
        }, {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        });
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const getListCertificate = async ()=>{
    try {
        return await api.get("certificates/");
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const getCertificate = async (id)=>{
    try {
        return await api.get(`certificates/${id}/`);
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const deleteCertificate = async (accessToken, id)=>{
    try {
        return await api.delete(`certificates/${id}/`, {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        });
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const getSlideList = async (accessToken)=>{
    try {
        return await api.get("slides/", {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        });
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const getSlide = async (accessToken, id)=>{
    try {
        return await api.get(`slides/${id}/`, {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        });
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const getQuizList = async (accessToken)=>{
    try {
        return await api.get("quizzes/", {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        });
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const getQuiz = async (accessToken, id)=>{
    try {
        return await api.get(`quizzes/${id}/`, {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        });
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const getExamList = async (accessToken)=>{
    try {
        return await api.get("exams/", {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        });
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const getExam = async (accessToken, id)=>{
    try {
        return await api.get(`exams/${id}/`, {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        });
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const deleteExam = async (accessToken, id)=>{
    try {
        return await api.delete(`exams/${id}/`, {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        });
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const getTutorialList = async (accessToken)=>{
    try {
        return await api.get("tutorials/", {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        });
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const getTutorial = async (accessToken, id)=>{
    try {
        return await api.get(`tutorials/${id}/`, {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        });
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const deleteTutorial = async (accessToken, id)=>{
    try {
        return await api.delete(`tutorials/${id}/`, {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        });
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const getProfile = async (accessToken)=>{
    try {
        return await api.get("profile", {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        });
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const getUserList = async (accessToken)=>{
    try {
        return await api.get("users", {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        });
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const deleteSlide = async (accessToken, id)=>{
    try {
        return await api.delete(`slides/${id}/`, {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        });
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const deleteQuiz = async (accessToken, id)=>{
    try {
        return await api.delete(`quizzes/${id}/`, {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        });
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};


/***/ })

};
;