exports.id = 725;
exports.ids = [725];
exports.modules = {

/***/ 3725:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6786);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(8038);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _LayoutPage_module_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3796);
/* harmony import */ var _LayoutPage_module_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_LayoutPage_module_css__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react_icons_md__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(4348);
/* harmony import */ var react_modal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(2251);
/* harmony import */ var react_modal__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_modal__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(1621);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_3__);






const LayoutPage = ({ name , columns , data , sendDelete , filter  })=>{
    const [modalIsOpen, setIsOpen] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
    const [currentItemId, setCurrentItemId] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(null);
    function openModal() {
        setIsOpen(true);
    }
    function afterOpenModal() {}
    function closeModal() {
        setIsOpen(false);
    }
    function handleText(text) {
        if (text === null) return "-";
        text = text.toString().split("_").join(" ");
        return text.toString().length > 25 ? text.toString().slice(0, 25) + "..." : text;
    }
    async function handleDelete() {
        sendDelete(currentItemId);
        setCurrentItemId(null);
        setIsOpen(false);
    }
    function handleDeleteItem(id) {
        console.log(id);
        setCurrentItemId(id);
        setIsOpen(true);
    }
    function handleItem(item) {
        if (Array.isArray(item)) return item.length;
        else return item;
    }
    return /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
        className: (_LayoutPage_module_css__WEBPACK_IMPORTED_MODULE_4___default().page),
        children: [
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                className: (_LayoutPage_module_css__WEBPACK_IMPORTED_MODULE_4___default().title),
                children: name
            }),
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                className: (_LayoutPage_module_css__WEBPACK_IMPORTED_MODULE_4___default().toolbar),
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                        children: "+ Add"
                    }),
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: (_LayoutPage_module_css__WEBPACK_IMPORTED_MODULE_4___default().showing),
                        children: [
                            "Showing 1 to ",
                            data.length
                        ]
                    }),
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: (_LayoutPage_module_css__WEBPACK_IMPORTED_MODULE_4___default().btns),
                        children: [
                            filter && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_icons_md__WEBPACK_IMPORTED_MODULE_5__/* .MdOutlineFilterAlt */ .V6e, {}),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_icons_md__WEBPACK_IMPORTED_MODULE_5__/* .MdOutlineArrowCircleDown */ .jB5, {})
                        ]
                    })
                ]
            }),
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                className: (_LayoutPage_module_css__WEBPACK_IMPORTED_MODULE_4___default().table),
                children: [
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: (_LayoutPage_module_css__WEBPACK_IMPORTED_MODULE_4___default().tablerow),
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: (_LayoutPage_module_css__WEBPACK_IMPORTED_MODULE_4___default().view),
                                children: "View"
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: (_LayoutPage_module_css__WEBPACK_IMPORTED_MODULE_4___default().fields),
                                children: columns.map((item, index)=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: (_LayoutPage_module_css__WEBPACK_IMPORTED_MODULE_4___default().col),
                                        children: handleText(item)
                                    }, index))
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: (_LayoutPage_module_css__WEBPACK_IMPORTED_MODULE_4___default().action),
                                children: "Action"
                            })
                        ]
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: (_LayoutPage_module_css__WEBPACK_IMPORTED_MODULE_4___default().databox),
                        children: data.map((item, index)=>/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: `${(_LayoutPage_module_css__WEBPACK_IMPORTED_MODULE_4___default().tablerow)} ${(_LayoutPage_module_css__WEBPACK_IMPORTED_MODULE_4___default().datarow)}`,
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_link__WEBPACK_IMPORTED_MODULE_3___default()), {
                                        className: (_LayoutPage_module_css__WEBPACK_IMPORTED_MODULE_4___default().view),
                                        href: `/admin/platform/${name}/${item.id}/view`,
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_icons_md__WEBPACK_IMPORTED_MODULE_5__/* .MdRemoveRedEye */ .FpO, {})
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: (_LayoutPage_module_css__WEBPACK_IMPORTED_MODULE_4___default().fields),
                                        children: columns.map((col, index)=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                className: (_LayoutPage_module_css__WEBPACK_IMPORTED_MODULE_4___default().field),
                                                children: handleText(handleItem(item[col]))
                                            }, index))
                                    }),
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: (_LayoutPage_module_css__WEBPACK_IMPORTED_MODULE_4___default().actiongroup),
                                        children: [
                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                className: `${(_LayoutPage_module_css__WEBPACK_IMPORTED_MODULE_4___default().actionbtn)} ${(_LayoutPage_module_css__WEBPACK_IMPORTED_MODULE_4___default().editbtn)}`,
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_icons_md__WEBPACK_IMPORTED_MODULE_5__/* .MdOutlineEditNote */ .E49, {}),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                        children: "Edit"
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                className: `${(_LayoutPage_module_css__WEBPACK_IMPORTED_MODULE_4___default().actionbtn)} ${(_LayoutPage_module_css__WEBPACK_IMPORTED_MODULE_4___default().deletebtn)}`,
                                                onClick: ()=>handleDeleteItem(item.id),
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_icons_md__WEBPACK_IMPORTED_MODULE_5__/* .MdOutlineDelete */ .$vK, {}),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                        children: "Delete"
                                                    })
                                                ]
                                            })
                                        ]
                                    })
                                ]
                            }, index))
                    })
                ]
            }),
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)((react_modal__WEBPACK_IMPORTED_MODULE_2___default()), {
                isOpen: modalIsOpen,
                onAfterOpen: afterOpenModal,
                onRequestClose: closeModal,
                contentLabel: "Delete Item",
                className: (_LayoutPage_module_css__WEBPACK_IMPORTED_MODULE_4___default().modal),
                ariaHideApp: false,
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: (_LayoutPage_module_css__WEBPACK_IMPORTED_MODULE_4___default().confirmtext),
                        children: "Delete this item"
                    }),
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: (_LayoutPage_module_css__WEBPACK_IMPORTED_MODULE_4___default().modalbtns),
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                onClick: closeModal,
                                children: "close"
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                className: (_LayoutPage_module_css__WEBPACK_IMPORTED_MODULE_4___default().deleteconfirm),
                                onClick: handleDelete,
                                children: "Delete"
                            })
                        ]
                    })
                ]
            })
        ]
    });
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (LayoutPage);


/***/ }),

/***/ 3796:
/***/ ((module) => {

// Exports
module.exports = {
	"page": "LayoutPage_page__1aAsp",
	"title": "LayoutPage_title__acVXH",
	"toolbar": "LayoutPage_toolbar__fFKUi",
	"table": "LayoutPage_table__baoVG",
	"tablerow": "LayoutPage_tablerow__1G_cB",
	"datarow": "LayoutPage_datarow__aA9nQ",
	"fields": "LayoutPage_fields__Fhwxj",
	"action": "LayoutPage_action__PGO_W",
	"col": "LayoutPage_col__Xeq5d",
	"field": "LayoutPage_field__9orZw",
	"view": "LayoutPage_view__wyjti",
	"actiongroup": "LayoutPage_actiongroup__HUL6B",
	"btns": "LayoutPage_btns__T94jU",
	"actionbtn": "LayoutPage_actionbtn__vsOoa",
	"deletebtn": "LayoutPage_deletebtn__DHYGL",
	"editbtn": "LayoutPage_editbtn__1wkUz",
	"databox": "LayoutPage_databox__1Dx9y",
	"loading": "LayoutPage_loading__LV2IW",
	"skeleton-loading": "LayoutPage_skeleton-loading__TEm1Z",
	"modal": "LayoutPage_modal__HrwaX",
	"deleteconfirm": "LayoutPage_deleteconfirm__6HMui",
	"modalbtns": "LayoutPage_modalbtns__3wdK4",
	"confirmtext": "LayoutPage_confirmtext__R66u_"
};


/***/ })

};
;