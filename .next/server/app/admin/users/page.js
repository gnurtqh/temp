(() => {
var exports = {};
exports.id = 674;
exports.ids = [674];
exports.modules = {

/***/ 7783:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/compiled/@edge-runtime/cookies");

/***/ }),

/***/ 8038:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/compiled/react");

/***/ }),

/***/ 8704:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/compiled/react-dom/server-rendering-stub");

/***/ }),

/***/ 7897:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/compiled/react-server-dom-webpack/client");

/***/ }),

/***/ 6786:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/compiled/react/jsx-runtime");

/***/ }),

/***/ 1090:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/server/app-render/get-segment-param.js");

/***/ }),

/***/ 8652:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/server/future/helpers/interception-routes.js");

/***/ }),

/***/ 3918:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/amp-context.js");

/***/ }),

/***/ 5732:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/amp-mode.js");

/***/ }),

/***/ 3280:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/app-router-context.js");

/***/ }),

/***/ 2796:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/head-manager-context.js");

/***/ }),

/***/ 9274:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/hooks-client-context.js");

/***/ }),

/***/ 4486:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/image-blur-svg.js");

/***/ }),

/***/ 744:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/image-config-context.js");

/***/ }),

/***/ 5843:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/image-config.js");

/***/ }),

/***/ 9552:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/image-loader");

/***/ }),

/***/ 4964:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router-context.js");

/***/ }),

/***/ 1751:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/add-path-prefix.js");

/***/ }),

/***/ 3938:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/format-url.js");

/***/ }),

/***/ 1668:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/handle-smooth-scroll.js");

/***/ }),

/***/ 1897:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/is-bot.js");

/***/ }),

/***/ 1109:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/is-local-url.js");

/***/ }),

/***/ 8854:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/parse-path.js");

/***/ }),

/***/ 3297:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/remove-trailing-slash.js");

/***/ }),

/***/ 7782:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/resolve-href.js");

/***/ }),

/***/ 3349:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/server-inserted-html.js");

/***/ }),

/***/ 2470:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/side-effect.js");

/***/ }),

/***/ 9232:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/utils.js");

/***/ }),

/***/ 618:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/utils/warn-once.js");

/***/ }),

/***/ 9491:
/***/ ((module) => {

"use strict";
module.exports = require("assert");

/***/ }),

/***/ 2361:
/***/ ((module) => {

"use strict";
module.exports = require("events");

/***/ }),

/***/ 7147:
/***/ ((module) => {

"use strict";
module.exports = require("fs");

/***/ }),

/***/ 3685:
/***/ ((module) => {

"use strict";
module.exports = require("http");

/***/ }),

/***/ 2241:
/***/ ((module) => {

"use strict";
module.exports = require("https");

/***/ }),

/***/ 2037:
/***/ ((module) => {

"use strict";
module.exports = require("os");

/***/ }),

/***/ 1017:
/***/ ((module) => {

"use strict";
module.exports = require("path");

/***/ }),

/***/ 2781:
/***/ ((module) => {

"use strict";
module.exports = require("stream");

/***/ }),

/***/ 6224:
/***/ ((module) => {

"use strict";
module.exports = require("tty");

/***/ }),

/***/ 7310:
/***/ ((module) => {

"use strict";
module.exports = require("url");

/***/ }),

/***/ 3837:
/***/ ((module) => {

"use strict";
module.exports = require("util");

/***/ }),

/***/ 9796:
/***/ ((module) => {

"use strict";
module.exports = require("zlib");

/***/ }),

/***/ 9734:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppRouter": () => (/* reexport default from dynamic */ next_dist_client_components_app_router__WEBPACK_IMPORTED_MODULE_0___default.a),
/* harmony export */   "GlobalError": () => (/* reexport default from dynamic */ next_dist_client_components_error_boundary__WEBPACK_IMPORTED_MODULE_3___default.a),
/* harmony export */   "LayoutRouter": () => (/* reexport default from dynamic */ next_dist_client_components_layout_router__WEBPACK_IMPORTED_MODULE_1___default.a),
/* harmony export */   "RenderFromTemplateContext": () => (/* reexport default from dynamic */ next_dist_client_components_render_from_template_context__WEBPACK_IMPORTED_MODULE_2___default.a),
/* harmony export */   "StaticGenerationSearchParamsBailoutProvider": () => (/* reexport default from dynamic */ next_dist_client_components_static_generation_searchparams_bailout_provider__WEBPACK_IMPORTED_MODULE_7___default.a),
/* harmony export */   "__next_app_webpack_require__": () => (/* binding */ __next_app_webpack_require__),
/* harmony export */   "createSearchParamsBailoutProxy": () => (/* reexport safe */ next_dist_client_components_searchparams_bailout_proxy__WEBPACK_IMPORTED_MODULE_8__.createSearchParamsBailoutProxy),
/* harmony export */   "decodeReply": () => (/* reexport safe */ next_dist_compiled_react_server_dom_webpack_server_edge__WEBPACK_IMPORTED_MODULE_10__.decodeReply),
/* harmony export */   "pages": () => (/* binding */ pages),
/* harmony export */   "renderToReadableStream": () => (/* reexport safe */ next_dist_compiled_react_server_dom_webpack_server_edge__WEBPACK_IMPORTED_MODULE_10__.renderToReadableStream),
/* harmony export */   "requestAsyncStorage": () => (/* reexport safe */ next_dist_client_components_request_async_storage__WEBPACK_IMPORTED_MODULE_5__.requestAsyncStorage),
/* harmony export */   "serverHooks": () => (/* reexport module object */ next_dist_client_components_hooks_server_context__WEBPACK_IMPORTED_MODULE_9__),
/* harmony export */   "staticGenerationAsyncStorage": () => (/* reexport safe */ next_dist_client_components_static_generation_async_storage__WEBPACK_IMPORTED_MODULE_4__.staticGenerationAsyncStorage),
/* harmony export */   "staticGenerationBailout": () => (/* reexport safe */ next_dist_client_components_static_generation_bailout__WEBPACK_IMPORTED_MODULE_6__.staticGenerationBailout),
/* harmony export */   "tree": () => (/* binding */ tree)
/* harmony export */ });
/* harmony import */ var next_dist_client_components_app_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8829);
/* harmony import */ var next_dist_client_components_app_router__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(next_dist_client_components_app_router__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_dist_client_components_layout_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5226);
/* harmony import */ var next_dist_client_components_layout_router__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_dist_client_components_layout_router__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_dist_client_components_render_from_template_context__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(2872);
/* harmony import */ var next_dist_client_components_render_from_template_context__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_dist_client_components_render_from_template_context__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_dist_client_components_error_boundary__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(8412);
/* harmony import */ var next_dist_client_components_error_boundary__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_dist_client_components_error_boundary__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var next_dist_client_components_static_generation_async_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(1839);
/* harmony import */ var next_dist_client_components_static_generation_async_storage__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_dist_client_components_static_generation_async_storage__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var next_dist_client_components_request_async_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(5120);
/* harmony import */ var next_dist_client_components_request_async_storage__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(next_dist_client_components_request_async_storage__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var next_dist_client_components_static_generation_bailout__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(9282);
/* harmony import */ var next_dist_client_components_static_generation_bailout__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(next_dist_client_components_static_generation_bailout__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var next_dist_client_components_static_generation_searchparams_bailout_provider__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(3785);
/* harmony import */ var next_dist_client_components_static_generation_searchparams_bailout_provider__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(next_dist_client_components_static_generation_searchparams_bailout_provider__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var next_dist_client_components_searchparams_bailout_proxy__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(5183);
/* harmony import */ var next_dist_client_components_searchparams_bailout_proxy__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(next_dist_client_components_searchparams_bailout_proxy__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var next_dist_client_components_hooks_server_context__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(5815);
/* harmony import */ var next_dist_client_components_hooks_server_context__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(next_dist_client_components_hooks_server_context__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var next_dist_compiled_react_server_dom_webpack_server_edge__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(6370);

    const tree = {
        children: [
        '',
        {
        children: [
        'admin',
        {
        children: [
        'users',
        {
        children: ['__PAGE__', {}, {
          page: [() => Promise.resolve(/* import() eager */).then(__webpack_require__.bind(__webpack_require__, 575)), "/home/hqtrung/code/quizzpool_fe/src/app/admin/users/page.js"],
          
        }]
      },
        {
          
          
        }
      ]
      },
        {
          'layout': [() => Promise.resolve(/* import() eager */).then(__webpack_require__.bind(__webpack_require__, 5734)), "/home/hqtrung/code/quizzpool_fe/src/app/admin/layout.js"],
          
        }
      ]
      },
        {
          'layout': [() => Promise.resolve(/* import() eager */).then(__webpack_require__.bind(__webpack_require__, 3175)), "/home/hqtrung/code/quizzpool_fe/src/app/layout.js"],
'loading': [() => Promise.resolve(/* import() eager */).then(__webpack_require__.bind(__webpack_require__, 6099)), "/home/hqtrung/code/quizzpool_fe/src/app/loading.js"],
          
        }
      ]
      }.children;
    const pages = ["/home/hqtrung/code/quizzpool_fe/src/app/admin/users/page.js"];

    
    
    
    

    

    

    
    
    

    

    
    const __next_app_webpack_require__ = __webpack_require__
  

/***/ }),

/***/ 3797:
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

Promise.resolve(/* import() eager */).then(__webpack_require__.t.bind(__webpack_require__, 4468, 23))

/***/ }),

/***/ 6060:
/***/ ((module) => {

// Exports
module.exports = {
	"page": "page_page__XGR2L",
	"title": "page_title__eY2i0",
	"toolbar": "page_toolbar__g6jvR",
	"btns": "page_btns__kCSPZ",
	"listuser": "page_listuser___mRCF"
};


/***/ }),

/***/ 575:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ users_page)
});

// EXTERNAL MODULE: ./node_modules/next/dist/compiled/react/jsx-runtime.js
var jsx_runtime = __webpack_require__(3146);
// EXTERNAL MODULE: ./node_modules/react-icons/md/index.esm.js
var index_esm = __webpack_require__(7731);
// EXTERNAL MODULE: ./src/app/admin/users/page.module.css
var page_module = __webpack_require__(6060);
var page_module_default = /*#__PURE__*/__webpack_require__.n(page_module);
// EXTERNAL MODULE: ./node_modules/next/headers.js
var headers = __webpack_require__(8558);
// EXTERNAL MODULE: ./node_modules/axios/dist/node/axios.cjs
var axios = __webpack_require__(6502);
var axios_default = /*#__PURE__*/__webpack_require__.n(axios);
;// CONCATENATED MODULE: ./src/connect/qpbe.js

const api = axios_default().create({
    baseURL: "https://quizzpool.smarthand.dev/api/"
});
const register = async (userData)=>{
    try {
        return await api.post("register", userData);
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const login = async (email, password)=>{
    try {
        return await api.post("login", {
            email,
            password
        });
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const refreshToken = async (refreshToken)=>{
    try {
        return await api.post("refresh_token", {
            refresh: refreshToken
        });
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const forgotPassword = async (email)=>{
    try {
        return await api.post("forgot_password", {
            email
        });
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const resetPassword = async (password, accessToken)=>{
    try {
        return await api.post("reset_password", {
            password
        }, {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        });
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const getListCertificate = async ()=>{
    try {
        return await api.get("certificates/");
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const getCertificate = async (id)=>{
    try {
        return await api.get(`certificates/${id}/`);
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const deleteCertificate = async (accessToken, id)=>{
    try {
        return await api.delete(`certificates/${id}/`, {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        });
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const getSlideList = async (accessToken)=>{
    try {
        return await api.get("slides/", {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        });
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const getSlide = async (accessToken, id)=>{
    try {
        return await api.get(`slides/${id}/`, {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        });
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const getQuizList = async (accessToken)=>{
    try {
        return await api.get("quizzes/", {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        });
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const getQuiz = async (accessToken, id)=>{
    try {
        return await api.get(`quizzes/${id}/`, {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        });
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const getExamList = async (accessToken)=>{
    try {
        return await api.get("exams/", {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        });
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const getExam = async (accessToken, id)=>{
    try {
        return await api.get(`exams/${id}/`, {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        });
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const deleteExam = async (accessToken, id)=>{
    try {
        return await api.delete(`exams/${id}/`, {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        });
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const getTutorialList = async (accessToken)=>{
    try {
        return await api.get("tutorials/", {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        });
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const getTutorial = async (accessToken, id)=>{
    try {
        return await api.get(`tutorials/${id}/`, {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        });
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const deleteTutorial = async (accessToken, id)=>{
    try {
        return await api.delete(`tutorials/${id}/`, {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        });
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const getProfile = async (accessToken)=>{
    try {
        return await api.get("profile", {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        });
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const getUserList = async (accessToken)=>{
    try {
        return await api.get("users", {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        });
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const deleteSlide = async (accessToken, id)=>{
    try {
        return await api.delete(`slides/${id}/`, {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        });
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};
const deleteQuiz = async (accessToken, id)=>{
    try {
        return await api.delete(`quizzes/${id}/`, {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        });
    } catch (error) {
        if (error.response) {
            return error.response;
        } else {
            console.log(error);
        }
    }
};

// EXTERNAL MODULE: ./node_modules/next/image.js
var next_image = __webpack_require__(2208);
var image_default = /*#__PURE__*/__webpack_require__.n(next_image);
;// CONCATENATED MODULE: ./src/app/admin/users/User.js


const User = ({ user  })=>{
    return /*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
        style: {
            boxShadow: "2px 2px 10px 4px rgba(50, 50, 50, 0.1)",
            padding: "2rem",
            borderRadius: "1rem",
            backgroundColor: "white",
            display: "flex",
            alignItems: "flex-end",
            gap: "1rem"
        },
        children: [
            /*#__PURE__*/ jsx_runtime.jsx((image_default()), {
                src: user.avatar_url,
                width: 100,
                height: 100,
                alt: "user avatar",
                priority: true
            }),
            /*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
                style: {
                    display: "flex",
                    flexDirection: "column",
                    gap: "0.5rem"
                },
                children: [
                    /*#__PURE__*/ jsx_runtime.jsx("div", {
                        children: user.username
                    }),
                    /*#__PURE__*/ jsx_runtime.jsx("div", {
                        children: user.email
                    }),
                    /*#__PURE__*/ jsx_runtime.jsx("div", {
                        children: user.role.toUpperCase()
                    })
                ]
            })
        ]
    });
};
/* harmony default export */ const users_User = (User);

;// CONCATENATED MODULE: ./src/app/admin/users/page.js






const page = async ()=>{
    const cookieStore = (0,headers.cookies)();
    const accessToken = cookieStore.get("access")?.value;
    const response = await getUserList(accessToken);
    return /*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
        className: (page_module_default()).page,
        children: [
            /*#__PURE__*/ jsx_runtime.jsx("div", {
                className: (page_module_default()).title,
                children: "Users"
            }),
            /*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
                className: (page_module_default()).toolbar,
                children: [
                    /*#__PURE__*/ jsx_runtime.jsx("button", {
                        children: "+ Add"
                    }),
                    /*#__PURE__*/ jsx_runtime.jsx("div", {
                        className: (page_module_default()).showing,
                        children: "Showing 1 to "
                    }),
                    /*#__PURE__*/ (0,jsx_runtime.jsxs)("div", {
                        className: (page_module_default()).btns,
                        children: [
                            /*#__PURE__*/ jsx_runtime.jsx(index_esm/* MdOutlineFilterAlt */.V6e, {}),
                            /*#__PURE__*/ jsx_runtime.jsx(index_esm/* MdOutlineArrowCircleDown */.jB5, {}),
                            /*#__PURE__*/ jsx_runtime.jsx(index_esm/* MdOutlineDelete */.$vK, {})
                        ]
                    })
                ]
            }),
            /*#__PURE__*/ jsx_runtime.jsx("div", {
                className: (page_module_default()).listuser,
                children: response.data.map((item)=>/*#__PURE__*/ jsx_runtime.jsx(users_User, {
                        user: item
                    }, item.email))
            })
        ]
    });
};
/* harmony default export */ const users_page = (page);


/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [67,272,27,744,897,348,904,795,394,494,352,498], () => (__webpack_exec__(9734)));
module.exports = __webpack_exports__;

})();